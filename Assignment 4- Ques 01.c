#include <stdio.h>

int main ()
{
  int n1, n2, n3, Sum;
  float Average ;

  printf ("Please Enter 3 integers :");
  scanf ("%d%d%d" , &n1, &n2, &n3);

  Sum = n1+n2+n3;
  Average =(n1+n2+n3)/3;

  printf ("\nSum is %d" , Sum);
  printf ("\nAverage is %.2f" , Average);

  return 0;

}
